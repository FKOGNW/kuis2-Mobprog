package id.kuis2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
                /*************************/
                /** Nama : Fiko Gunawan **/
                /** NIM  : 1502144      **/
                /*************************/


    private Button btRegister, btCekSaldo, btTransfer, btLogOut;
    private TextView ID;

    SharedPreferences session;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES="MyPref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btRegister= findViewById(R.id.btRegister);
        btCekSaldo= findViewById(R.id.btCelSaldo);
        btTransfer= findViewById(R.id.btTransfer);
        btLogOut= findViewById(R.id.btLogOut);

        ID= findViewById(R.id.tvID);

        /*session*/
        session= getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = session.edit();

        String tempID= session.getString("ID", "");



        ID.setText("ID : "+tempID);

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRegister();
            }
        });

        btCekSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goTocekSaldo();
            }
        });

        btTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToTransfer();
            }
        });

        btLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });
    }

    private void logOut() {
        editor.clear();
        editor.commit();
        finish();
    }

    private void goToTransfer() {
        startActivity(new Intent(this, TransferActivity.class));
    }

    private void goTocekSaldo() {
        startActivity(new Intent(this, SaldoActivity.class));
    }

    private void goToRegister() {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    /**
     * override backbutton
     * */
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        finish();
    }


}
