package id.kuis2.api;

import java.util.List;

import id.kuis2.model.Accounts;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    /**
     * POST akun*/
    @POST("buat_account")
    Call<Accounts> postUser(@Query("id") String id, @Query("passwd") String passwd);

    /**
     * GET saldo*/
    @GET("cek_saldo")
    Call<Accounts> getSaldo(@Query("id") String id, @Query("passwd") String passwd);

    /**
     * POST transfer*/
    @POST("transfer_coin")
    Call<Accounts> postTransfer(@Query("id") String id, @Query("passwd") String passwd,
                                @Query("id_tujuan") String id_tujuan, @Query("Jumlah") Integer jumlah);
}
