package id.kuis2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import id.kuis2.api.ApiClient;
import id.kuis2.api.ApiInterface;
import id.kuis2.model.Accounts;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.kuis2.MainActivity.MyPREFERENCES;

public class SaldoActivity extends AppCompatActivity {
    /*************************/
    /** Nama : Fiko Gunawan **/
    /** NIM  : 1502144      **/
    /*************************/

    private Accounts account;
    private TextView tvSaldo;
    private Button btOk;
    private int saldo;

    SharedPreferences session;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saldo);

        tvSaldo= findViewById(R.id.tvSaldo);
//        btOk= findViewById(R.id.btCelSaldo);

        /*session login*/
        session= getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        final String tempUsername= session.getString("Name", "");
        final String tempPassword= session.getString("Password", "");

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//
        // Calling Method for Retrofit
        // response : GET User from Accounts
        Call<Accounts> call = apiService.getSaldo(tempUsername, tempPassword);

        // Calling Method for Retrofit
        // response : GET User from Accounts
        call.enqueue(new Callback<Accounts>() {
            @Override
            public void onResponse(@NonNull Call<Accounts> call, @NonNull Response<Accounts> response) {
                account = response.body();
//                if (account.size()!= 0){
                  tvSaldo.setText("Rp."+account.getSaldo());
//                }
                Log.i("FK", tempUsername);
            }

            @Override
            public void onFailure(Call<Accounts> call, Throwable t) {
                Log.e("FK", "onFailure: ", t);
                Toast.makeText(SaldoActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
            }


        });


    }
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        finish();
    }
}
