package id.kuis2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import id.kuis2.api.ApiClient;
import id.kuis2.api.ApiInterface;
import id.kuis2.model.Accounts;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.kuis2.MainActivity.MyPREFERENCES;

public class TransferActivity extends AppCompatActivity {

    /*************************/
    /** Nama : Fiko Gunawan **/
    /** NIM  : 1502144      **/
    /*************************/
    private EditText etIdTujuan, etNominal;
    private Button btTransfer;

    private String idTujuan;
    private Integer nominal;

    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        etIdTujuan= findViewById(R.id.etIdTujuan);
        etNominal= findViewById(R.id.etNominal);

        btTransfer= findViewById(R.id.btTransfer);

        session= getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        final String id= session.getString("ID", "");
        final String passwd= session.getString("Password", "");

        btTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                idTujuan= String.valueOf(etIdTujuan.getText());
                String temp= String.valueOf(etNominal.getText());
                nominal.parseInt(String.valueOf(etNominal.getText()));

                if(idTujuan.trim().length()>0 && temp.trim().length()>0){
                    // Retrofit for POST User Info

                    // Initialize the Retrofit Interface
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    Call<Accounts> call = apiService.postTransfer(id, passwd, idTujuan, nominal);
                    Log.i("FK", id);

                    call.enqueue(new Callback<Accounts>() {
                        @Override
                        public void onResponse(@NonNull Call<Accounts> call, @NonNull Response<Accounts> response) {
//                            Accounts account = response.body();

                            launchMainScreen();
                        }

                        @Override
                        public void onFailure(Call<Accounts> call, Throwable t) {
                            Log.e("FK", "onFailure: ", t);
                            Toast.makeText(TransferActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                        }
                    });
                }else{
                    Toast.makeText(TransferActivity.this, "Tolong lengkapi terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    private void launchMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        finish();
    }
}
