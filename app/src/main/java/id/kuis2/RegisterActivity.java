package id.kuis2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import id.kuis2.api.ApiClient;
import id.kuis2.api.ApiInterface;
import id.kuis2.model.Accounts;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.kuis2.MainActivity.MyPREFERENCES;

public class RegisterActivity extends AppCompatActivity {
    /*************************/
    /** Nama : Fiko Gunawan **/
    /** NIM  : 1502144      **/
    /*************************/

    private EditText etID, etPassword;
    private Button btRegister;

    private String ID, password;

    SharedPreferences session;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etID= findViewById(R.id.etID);
        etPassword= findViewById(R.id.etPassword);
        btRegister= findViewById(R.id.btRegister);



        /*session register*/
        session= getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ID= String.valueOf(etID.getText());
                password= String.valueOf(etPassword.getText());
//                Toast.makeText(RegisterActivity.this, ID, Toast.LENGTH_SHORT).show();

                if(ID.trim().length()>0 && password.trim().length()>0) {

                    // Retrofit for POST User Info

                    // Initialize the Retrofit Interface
                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

                    Call<Accounts> call = apiService.postUser(ID, password);

                    call.enqueue(new Callback<Accounts>() {
                        @Override
                        public void onResponse(@NonNull Call<Accounts> call, @NonNull Response<Accounts> response) {
//                            Accounts account = response.body();
                            editor = session.edit();

                            editor.putString("ID", ID);
                            editor.putString("Password", password);

                            Log.i("FK", password);

                            editor.commit();
                            launchMainScreen();
                        }

                        @Override
                        public void onFailure(Call<Accounts> call, Throwable t) {
                            Log.e("FK", "onFailure: ", t);
                            Toast.makeText(RegisterActivity.this, "Ada Yang Salah", Toast.LENGTH_LONG).show();
                        }
                    });


                }else{
                    Toast.makeText(RegisterActivity.this, "Harap Lengkapi terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void launchMainScreen() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
    @Override
    public void onBackPressed() {
//        Log.d("CDA", "onBackPressed Called");
        finish();
    }
}
